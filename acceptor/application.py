"""FIX Application"""
import quickfix as fix
import logging
import time
from model.logger import setup_logger
__SOH__ = chr(1)
import quickfix44
from datetime import date, datetime

setup_logger('logfix', 'Logs/message.log')
logfix = logging.getLogger('logfix')

class Application(fix.Application):
    """FIX Application"""
    orderID = 0
    execID = 0


    def onCreate(self, sessionID):
        """onCreate"""
        print("onCreate : Session (%s)" % sessionID.toString())
        return

    def onLogon(self, sessionID):
        """onLogon"""
        self.sessionID = sessionID
        print("Successful Logon to session '%s'." % sessionID.toString())
        return

    def onLogout(self, sessionID):
        """onLogout"""
        print("Session (%s) logout !" % sessionID.toString())
        return

    def toAdmin(self, message, sessionID):
        msg = message.toString().replace(__SOH__, "|")
        logfix.debug("(Admin) S >> %s" % msg)
        return

    def fromAdmin(self, message, sessionID):
        msg = message.toString().replace(__SOH__, "|")
        logfix.debug("(Admin) R << %s" % msg)
        return

    def toApp(self, message, sessionID):
        msg = message.toString().replace(__SOH__, "|")
        #logfix.debug("(App) S >> %s" % msg)
        return

    def get_header_field_value(self, fobj, msg):
       
        if msg.getHeader().isSetField(fobj.getField()):
            msg.getHeader().getField(fobj)
            return fobj.getValue()
        else:
            return None

    def get_field_value(self, fobj, msg):
        if msg.isSetField(fobj.getField()):
            msg.getField(fobj)
            return fobj.getValue()
        else:
            return None

    def fromApp(self, message, sessionID):
        msg = message.toString().replace(__SOH__, "|")
        #logfix.debug("(App) R << %s" % msg)
        msg_type = self.get_header_field_value(fix.MsgType(), message)

        if  msg_type == fix.MsgType_QuoteRequest:
            self.send_Quote(message, sessionID)
        elif msg_type == fix.MsgType_NewOrderSingle:
            print("Message Type : NewOrderSingle")
            self.send_executionReport_OrderReceived(message,sessionID)
            time.sleep(1)
            self.send_executionReport_OrderExecuted(message,sessionID)
        else:
            print("Unprocessed Message Type")

        return

    def send_executionReport_OrderExecuted(self, message, sessionID):
        print("Sending ExecutionReport 'OrderExecuted'")

        price = 0
        quantity = 100_000
        last_quantity = 0
        leaves_quantity = 0
        avg_px = 0
        last_px = 108.85574
        last_spot_rate = 104.23

        utc_time = datetime.utcnow().strftime("%Y%m%d-%H:%M:%S.%f")[:-3]

        executionReport = fix.Message()
        executionReport.getHeader().setField( fix.MsgType(fix.MsgType_ExecutionReport) )

        account, cl_order_id, currency,  order_qty, side, symbol, settldate = (     fix.Account(), 
                                                                                    fix.ClOrdID(),
                                                                                    fix.Currency(), 
                                                                                    fix.OrderQty(), 
                                                                                    fix.Side(), 
                                                                                    fix.Symbol(),
                                                                                    fix.SettlDate() )

        message.getField( account )
        message.getField( cl_order_id )
        message.getField( currency )
        message.getField( order_qty )
        message.getField( side )
        message.getField( symbol )
        message.getField( settldate )

        print("Account: " + str(account.getString()))
        print("ClOrdID: " + str(cl_order_id.getString()))
        print("Currency: " + str(currency.getString()))
        print("OrderQty: " + str(order_qty.getString()))
        print("Side: " + str(side.getString()))
        print("Symbol: " + str(symbol.getString()))
        print("SettlDate: " + str(settldate.getString()))

        exec_id = self.genExecID()
        order_id = self.genOrderID()

        transaction_time = fix.TransactTime()
        transaction_time.setString(utc_time)

        executionReport.setField( account )                                     # Account
        executionReport.setField( fix.AvgPx(avg_px))                            # AvgPx
        executionReport.setField( cl_order_id )                                 # ClOrdID
        executionReport.setField( fix.CumQty(quantity))                         # CumQty
        executionReport.setField( currency)                                     # Currency
        executionReport.setField( fix.ExecID(exec_id) )                         # ExecID
        executionReport.setField( fix.LastPx(last_px))                          # LastPx -> New
        executionReport.setField( fix.LastQty(last_quantity))                   # LastQty
        executionReport.setField( fix.OrderID(order_id) )                       # OrderID
        executionReport.setField( order_qty )                                   # OrderQty 
        executionReport.setField( fix.OrdStatus(fix.OrdStatus_FILLED) )         # OrdStatus 
        executionReport.setField( fix.OrdType(fix.OrdType_PREVIOUSLY_QUOTED))   # OrdType
        # executionReport.setField( fix.Price(price) )                            # Price
        executionReport.setField( side )                                        # Side
        executionReport.setField( symbol )                                      # Symbol    
        executionReport.setField( transaction_time)                              # TransactTime -> new
        # executionReport.setField( fix.Text("NewOrderSingle received " + str(datetime.now())))
        executionReport.setField( settldate)                                    # SettlDate
        executionReport.setField( fix.ExecType(fix.ExecType_TRADE) )            # ExecType
        executionReport.setField( fix.LeavesQty(leaves_quantity) )              # LeavesQty
        executionReport.setField( fix.LastSpotRate(last_spot_rate))             # LastSpotRate -> New

        print("")
        print("Sending Quote with:")
        print("ExecID:" + str(exec_id))
        print("OrderID:" + str(order_id))
        print("AvgPx"+ str(avg_px))
        print("CumQty:" + str(quantity))
        print("LeavesQty:" + str(leaves_quantity))
        print("OrdStatus: " + str(fix.OrdStatus_FILLED))
        print("ExecType: " + str(fix.ExecType_TRADE))
        print("LastPx: " + str(last_px))
        print("LastSpotRate: " + str(last_spot_rate))
        print("TransactionTime: " + str(utc_time))
        print("")

        try:
            fix.Session.sendToTarget( executionReport, sessionID )
            print("Successful send Execution Report - Order Executed")
        except fix.SessionNotFound as e:
            print(str(e))
        return

    def send_executionReport_OrderReceived(self, message, sessionID):
        print("Sending ExecutionReport 'OrderReceived'")

        price = 0
        quantity = 0
        exec_id = str(0)

        executionReport = fix.Message()
        executionReport.getHeader().setField( fix.MsgType(fix.MsgType_ExecutionReport) )

        account, cl_order_id, currency,  order_qty, side, symbol, settldate = (     fix.Account(), 
                                                                                    fix.ClOrdID(),
                                                                                    fix.Currency(), 
                                                                                    fix.OrderQty(), 
                                                                                    fix.Side(), 
                                                                                    fix.Symbol(),
                                                                                    fix.SettlDate() )

        message.getField( account )
        message.getField( cl_order_id )
        message.getField( currency )
        message.getField( order_qty )
        message.getField( side )
        message.getField( symbol )
        message.getField( settldate )

        print("Account: " + str(account.getString()))
        print("ClOrdID: " + str(cl_order_id.getString()))
        print("Currency: " + str(currency.getString()))
        print("OrderQty: " + str(order_qty.getString()))
        print("Side: " + str(side.getString()))
        print("Symbol: " + str(symbol.getString()))
        print("SettlDate: " + str(settldate.getString()))

        order_id = self.genOrderID()

        executionReport.setField( account )                                     # Account
        executionReport.setField( fix.AvgPx(price))                             # AvgPx
        executionReport.setField( cl_order_id )                                 # ClOrdID
        executionReport.setField( fix.CumQty(quantity))                         # CumQty
        executionReport.setField( currency)                                     # Currency
        executionReport.setField( fix.ExecID(exec_id) )                         # ExecID
        executionReport.setField( fix.LastQty(quantity))                        # LastQty
        executionReport.setField( fix.OrderID(order_id) )                       # OrderID
        executionReport.setField( order_qty )                                   # OrderQty 
        executionReport.setField( fix.OrdStatus(fix.OrdStatus_NEW) )            # OrdStatus 
        executionReport.setField( fix.OrdType(fix.OrdType_PREVIOUSLY_QUOTED))   # OrdType
        executionReport.setField( fix.Price(price) )                            # Price
        executionReport.setField( side )                                        # Side
        executionReport.setField( symbol )                                      # Symbol    
        executionReport.setField( fix.Text("NewOrderSingle received " + str(datetime.now())))
        executionReport.setField( settldate)                                    # SettlDate
        executionReport.setField( fix.ExecType(fix.ExecType_NEW) )              # ExecType
        executionReport.setField( fix.LeavesQty(quantity) )                     # LeavesQty

        print("")
        print("Sending Quote with:")
        print("ExecID:" + str(exec_id))
        print("OrderID:" + str(order_id))
        print("Price:" + str(price))
        print("AvgPx: "+ str(price))
        print("CumQty:" + str(price))
        print("LeavesQty:" + str(quantity))
        print("LastQty:" + str(quantity))
        print("OrdStatus: " + str(fix.OrdStatus_NEW))
        print("ExecType: " + str(fix.ExecType_NEW))
        print("")


        try:
            fix.Session.sendToTarget( executionReport, sessionID )
            print("Successful send Execution Report - Order Received")
        except fix.SessionNotFound as e:
            print(str(e))
        return

    def send_Quote(self, message, sessionID):
        print("")
        print("Message Type : QuoteRequest")

        response_msg = fix.Message()
        header = response_msg.getHeader()
        header.setField(fix.MsgType(fix.MsgType_Quote))

        quote_req_id = fix.QuoteReqID()
        message.getField(quote_req_id)
        print("QuoteReqID: " + str(quote_req_id.getString()))

        symbol, quote_type, side, order_qty, currency, account = fix.Symbol(), fix.QuoteType(), fix.Side(), fix.OrderQty(), fix.Currency(), fix.Account()

        group = quickfix44.QuoteRequest.NoRelatedSym()
        message.getGroup(1, group)
        group.getField(quote_type)
        print("QuoteType: " + str(quote_type.getString()))
        group.getField(side)
        print("Side: " + str(side.getString()))
        group.getField(order_qty)
        print("OrderQty: " + str(order_qty.getString()))
        group.getField(symbol)
        print("Symbol: " + str(symbol.getString()))
        group.getField(currency)
        print("Currency: " + str(currency.getString()))
        group.getField(account)
        print("Account: " + str(account.getString()))


        quote_id = str(quote_req_id.getString()) + '-MOCK-SERVER-' + str(date.today())
        offer_spot_rate = 1.40
        issuer = 'Barclays BARX.DEMO'
        spot_date = "20220706"

        response_msg.setField(account)
        response_msg.setField(currency)
        response_msg.setField(order_qty)
        response_msg.setField(symbol)
        response_msg.setField(fix.Issuer(issuer))
        response_msg.setField(fix.QuoteID(quote_id))
        response_msg.setField(quote_req_id)
        response_msg.setField(fix.OfferSpotRate(offer_spot_rate))

        ref_spot_date = fix.StringField(7070,spot_date)
        response_msg.setField(ref_spot_date)
        # response_msg.setField(ref_spot_date) not impl. bcs. RefSpotDate custom field

        print("")
        print("Sending Quote with:")
        print("QuoteID: " + quote_id)
        print("OfferSpotRate: " + str(offer_spot_rate))
        print("Issuer: " + issuer)
        print("RefSpotDate: " + spot_date)
        print("")

        try:
            fix.Session.sendToTarget( response_msg, sessionID )
        except fix.SessionNotFound as e:
            return
        return 

    def genOrderID(self):
        self.orderID += 1
        return str(self.orderID).zfill(5)

    def genExecID(self):
        self.execID += 1
        return str(self.execID).zfill(5)

    def run(self):
        """Run"""
        while 1:
            time.sleep(2)
