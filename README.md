# Test Engine QuickFIX Mock Server #

## Requirements
* Python 3.x
* [QuickFIX Engine 1.15.1](http://www.quickfixengine.org/)

## Installing Requirements
```
pip install -r requirements.txt
```

## Run Project
### Without Docker
```sh
Please modify file path in acceptor/client.cfg accordingly.
```
```sh
cd ./acceptor
python server.py server.cfg
```


